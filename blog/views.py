from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect


def loginclient(request):
    context = {'title':'Login do client'}
    return render(request, 'loginclient/index.html', context)

@login_required()
def home(request):
    context = {'title':'Login do client'}
    return render(request, 'loginclient/home.html', context)


def google(request):
    context = {'title':'Login do client'}
    return render(request, 'googlee3ec6212ee392848.html', context)