from django.conf.global_settings import DATABASES

from .base import *
import dj_database_url

ENVIRONMENT = 'production'
DEBUG = True
ALLOWED_HOSTS = ['*']
DATABASES['default'] = dj_database_url.config(
    default='postgres://vktpvkoowitgyz:b17dd563cb0ff6708035f7a38c0a988b4cfdeabcdcfad165b58937b85ffdbf3f@ec2-75-101-142-91.compute-1.amazonaws.com:5432/d33f4ccq4l6dn7'
)
